//
// Copyright (c) 2016-2019 Vinnie Falco (vinnie dot falco at gmail dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
// Official repository: https://github.com/CPPAlliance/http_proto
//

#ifndef BOOST_HTTP_PROTO_RFC_CHARSETS_HPP
#define BOOST_HTTP_PROTO_RFC_CHARSETS_HPP

#include <boost/http_proto/detail/config.hpp>
#include <boost/url/bnf/charset.hpp>

namespace boost {
namespace http_proto {

} // http_proto
} // boost

#endif
